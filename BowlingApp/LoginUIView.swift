//
//  ContentView.swift
//  BowlingApp
//
//  Created by Chris on 2022/1/22.
//

import SwiftUI

struct LoginUIView: View {
    @EnvironmentObject var viewModel: ViewModel
    var body: some View {
        if viewModel.uiNumber == 1{
            VStack{
                Image("bowling")
                    .resizable()
                    .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                    .scaledToFit()
                    .frame(width: 300.0, height: 300.0)
                                .onTapGesture() {
                                    viewModel.onHomeUI()
                                }
                Button(action: {
                    viewModel.onHomeUI()
                }) {
                    HStack{
                        Image(systemName: "f.square.fill")
                            .font(.title)
                        Text("Facebook")
                            .fontWeight(.semibold)
                            .font(.title)
                    }
                }.buttonStyle(GradientBackgroundStyle())
                Button(action: {
                    viewModel.onHomeUI()
                }) {
                    HStack{
                        Image(systemName: "g.square.fill")
                            .font(.title)
                        Text("Google")
                            .fontWeight(.semibold)
                            .font(.title)
                    }
                }.buttonStyle(GradientBackgroundStyle())
            }
        }else{
                NavigationUIView()
                    .environmentObject(viewModel)
            
           
        }
    }
}

struct LoginUIView_Previews: PreviewProvider {
    static var previews: some View {
        LoginUIView()
        
    }
}
struct Model {
    private(set) var uiNumber:Int = 0
    
    private(set) var isJump: Bool = false
    mutating func toggleIsJump() {
        isJump.toggle()
    }
    mutating func loadingUI() {
        uiNumber = 0
    }
    mutating func signUI() {
        uiNumber = 1
    }
    mutating func homeUI() {
        uiNumber = 2
    }
}
class ViewModel: ObservableObject {
    @Published private var model = Model()
    var isJump: Bool {
        model.isJump
    }
    var uiNumber:Int{
        model.uiNumber
    }
    func toggleIsJump() {
        model.toggleIsJump()
    }
    func onLoginUI() {
        model.signUI()
    }
    func onLoadingUI() {
        model.loadingUI()
    }
    func onHomeUI() {
        model.homeUI()
    }
}
struct GradientBackgroundStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(minWidth: 0, maxWidth: .infinity)
            .padding()
            .foregroundColor(.white)
            .background(LinearGradient(gradient: Gradient(colors: [Color("DarkGreen"), Color("LightGreen")]), startPoint: .leading, endPoint: .trailing))
            .cornerRadius(40)
            .padding(.horizontal, 20)
    }
}
