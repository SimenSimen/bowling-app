//
//  BowlingAppApp.swift
//  BowlingApp
//
//  Created by Chris on 2022/1/22.
//

import SwiftUI

@main
struct BowlingAppApp: App {
    var body: some Scene {
        WindowGroup {
            LoadingView()
        }
    }
}
