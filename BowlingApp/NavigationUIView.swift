//
//  HomeView.swift
//  BowlingApp
//
//  Created by Chris on 2022/1/27.
//

import SwiftUI

struct NavigationUIView: View {
    @EnvironmentObject var viewModel: ViewModel
    var body: some View {
        if viewModel.uiNumber == 2{
            TabView {
                GameUIView()
                    .tabItem {
                        Image(systemName: "gamecontroller.fill")
                        Text("比賽")
                    }
                HomeUIView()
                    .tabItem {
                        Image(systemName: "house.fill")
                        Text("主頁")
                    }
                MemberInfoUIView()
                    .tabItem {
                        Image(systemName: "info.circle.fill")
                        Text("個人")
                    }
            }
        }
    }
}

struct NavigationUIView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationUIView()
    }
}
