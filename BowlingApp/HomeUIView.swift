//
//  HomeUIView.swift
//  BowlingApp
//
//  Created by Chris on 2022/1/27.
//

import SwiftUI

struct HomeUIView: View {
    
    var body: some View {
            VStack{
                List {
                    Text("1")
                    Text("2")
                    Text("3")
                }
                .frame(height: 200.0)
                VStack{
                    HStack{
                        GroupBox(label: Text("勝場")) {
                            Text("9999")
                        }
                        GroupBox(label: Text("勝場")) {
                            Text("9999")
                        }
                    }
                    HStack{
                        GroupBox(label: Text("勝場")) {
                            Text("9999")
                        }
                        GroupBox(label: Text("勝場")) {
                            Text("9999")
                        }
                    }
                    HStack{
                        GroupBox(label: Text("勝場")) {
                            Text("9999")
                        }
                        GroupBox(label: Text("勝場")) {
                            Text("9999")
                        }
                    }
                    HStack{
                        GroupBox(label: Text("勝場")) {
                            Text("9999")
                        }
                        GroupBox(label: Text("勝場")) {
                            Text("9999")
                        }
                    }
                }.padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                Spacer()
            
        }
       
    }
}

struct HomeUIView_Previews: PreviewProvider {
    static var previews: some View {
        HomeUIView()
    }
}
