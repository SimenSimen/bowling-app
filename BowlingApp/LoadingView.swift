//
//  LoadingView.swift
//  BowlingApp
//
//  Created by Chris on 2022/1/25.
//

import SwiftUI

struct LoadingView: View {
    @ObservedObject var viewModel = ViewModel()
    var body: some View {
        if viewModel.uiNumber == 0{
                VStack(alignment: .center){
                    Image("bowling")
                        .resizable()
                        .padding(.all)
                        .scaledToFit()
                        .frame(width: 300.0, height: 300.0)
                        .onTapGesture() {
                            viewModel.onLoginUI()
                        }
                    Text("點擊圖片登入")
                }
        }else{
            LoginUIView()
                .environmentObject(viewModel)
        }
        
        
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
